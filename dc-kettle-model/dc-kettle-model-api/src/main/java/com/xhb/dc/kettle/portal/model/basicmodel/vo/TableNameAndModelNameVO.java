package com.xhb.dc.kettle.portal.model.basicmodel.vo;

import lombok.Data;

/**
 * TableNameAndModelNameVO.
 */
@Data
public class TableNameAndModelNameVO {

    private String tableName;

    private String modelName;

}
